package com.forex.forexapi;

import java.math.BigDecimal;

public class AdvancedConvertedBean {
    private String convertedFrom;
    private String convertedTo;
    private BigDecimal rate;
    private BigDecimal convertedAmount;

    public AdvancedConvertedBean() {
    }

    public AdvancedConvertedBean(String convertedFrom, String convertedTo, BigDecimal rate, BigDecimal convertedAmount) {
        this.convertedFrom = convertedFrom;
        this.convertedTo = convertedTo;
        this.rate = rate;
        this.convertedAmount = convertedAmount;
    }

    public String getConvertedFrom() {
        return convertedFrom;
    }

    public void setConvertedFrom(String convertedFrom) {
        this.convertedFrom = convertedFrom;
    }

    public String getConvertedTo() {
        return convertedTo;
    }

    public void setConvertedTo(String convertedTo) {
        this.convertedTo = convertedTo;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal  rate) {
        this.rate = rate;
    }

    public BigDecimal getConvertedAmount() {
        return convertedAmount;
    }

    public void setConvertedAmount(BigDecimal convertedAmount) {
        this.convertedAmount = convertedAmount;
    }
}
