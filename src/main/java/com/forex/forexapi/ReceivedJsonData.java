package com.forex.forexapi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;
import java.util.HashMap;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReceivedJsonData {
    private String status;
    private String timestamp;
    private String base;
    private String date;
    private HashMap<String, BigDecimal> rates;

    public ReceivedJsonData() {
    }

    public ReceivedJsonData(String status, String timestamp, String base, String date, HashMap<String, BigDecimal> rates) {
        this.status = status;
        this.timestamp = timestamp;
        this.base = base;
        this.date = date;
        this.rates = rates;
    }

    public ReceivedJsonData(HashMap<String, BigDecimal> rates) {
        this.rates = rates;
        this.status = null;
        this.timestamp = null;
        this.base = null;
        this.date = null;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public HashMap<String, BigDecimal> getRates() {
        return rates;
    }

    public void setRates(HashMap<String, BigDecimal> rates) {
        this.rates = rates;
    }

    @Override
    public String toString() {
        return "ReceivedJsonData{" +
                "status='" + status + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", base='" + base + '\'' +
                ", date='" + date + '\'' +
                ", rates=" + rates +
                '}';
    }
}

