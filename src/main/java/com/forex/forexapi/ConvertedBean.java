package com.forex.forexapi;

import java.math.BigDecimal;

public class ConvertedBean {
    private String convertedTo;
    private BigDecimal convertedAmount;

    public ConvertedBean() {
    }

    public ConvertedBean(String convertedTo, BigDecimal convertedAmount) {
        this.convertedTo = convertedTo;
        this.convertedAmount = convertedAmount;
    }

    public String getConvertedTo() {
        return convertedTo;
    }

    public void setConvertedTo(String convertedTo) {
        this.convertedTo = convertedTo;
    }

    public BigDecimal getConvertedAmount() {
        return convertedAmount;
    }

    public void setConvertedAmount(BigDecimal convertedAmount) {
        this.convertedAmount = convertedAmount;
    }
}
