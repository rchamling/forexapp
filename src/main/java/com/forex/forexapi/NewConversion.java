package com.forex.forexapi;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class NewConversion {

    private BigDecimal rate;

    public NewConversion() {
    }

    public NewConversion(BigDecimal rate) {
        this.rate = rate;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }
}
