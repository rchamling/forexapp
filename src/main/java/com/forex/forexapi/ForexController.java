package com.forex.forexapi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Optional;

import static java.lang.Integer.parseInt;

@RestController
public class ForexController {

    private static final Logger log = LoggerFactory.getLogger(ForexController.class);

    private RestTemplate restTemplate = new RestTemplate();

    @GetMapping("/EUR/{target}/amount/{amount}")
    public ConvertedBean convertedBean(@PathVariable("target") String target, @PathVariable("amount") BigDecimal amount) {
        ReceivedJsonData data = restTemplate.getForObject("http://data.fixer.io/api/latest?access_key=1a3aeb19d2f23f315542516fbc32076b&symbols=" + target, ReceivedJsonData.class);
//        ReceivedJsonData data = restTemplate.getForObject("https://free.currconv.com/api/v7/convert?q="+source+"_"+target+"&compact=ultra&apiKey=11755782fc4a807beb04", ReceivedJsonData.class);
        if (data != null) {
            log.info(data.toString());
            BigDecimal rate = data.getRates().get(target);
            return new ConvertedBean(target, amount.multiply(rate));
        }
        return null;
    }

    @GetMapping("/{source}/to/{target}/amount/{amount}")
    public AdvancedConvertedBean advancedConvertedBean(@PathVariable("source") String source, @PathVariable("target") String target, @PathVariable("amount") BigDecimal amount) {
        String query = source + "_" + target;
        HashMap<String, Double> data = (HashMap<String, Double>) restTemplate.getForObject("https://free.currconv.com/api/v7/convert?q=" + query + "&compact=ultra&apiKey=11755782fc4a807beb04", Object.class);
//        NewConversion data = restTemplate.getForObject("https://free.currconv.com/api/v7/convert?q=" + query + "&compact=ultra&apiKey=11755782fc4a807beb04", NewConversion.class);

        BigDecimal rate = new BigDecimal(data.get(query));
//        BigDecimal rate = null;
//        if (data != null) {
//            rate = data.getRate();
//        }

        return new AdvancedConvertedBean(source, target, rate, amount.multiply(rate));

    }
}
